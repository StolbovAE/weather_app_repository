package com.example.weatherapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private EditText user_field;
    private Button searchButton;
    private TextView result_info;
    public TextView result_img_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

//        if (android.os.Build.VERSION.SDK_INT > 9) {
//            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//            StrictMode.setThreadPolicy(policy);
//        }
        user_field = findViewById(R.id.user_field);
        searchButton = findViewById(R.id.searchButton);
        result_info = findViewById(R.id.result_info);
        result_img_info = findViewById(R.id.result_img_info);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user_field.getText().toString().trim().equals("")) {
                    Toast.makeText(MainActivity.this, R.string.no_user_input, Toast.LENGTH_LONG).show();
                }
                else {

                    String city = user_field.getText().toString();
                    String key = "3e24d83014b0571051c1a54fd59ec934";
                    String url = "https://yandex.ru/pogoda/" + city;
                    String imgUrl = "https://yandex.ru/images/search?from=tabbar&text=" + city;

                    new GetUrlData().execute(url);
                    new GetUrlImgData().execute(imgUrl);
                }
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class GetUrlData extends AsyncTask<String, String, String> {

        private boolean w_result = false;

        public boolean getResult() {
            return w_result;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            result_info.setText("Ожидайте...");
        }

        @Override
        protected String doInBackground(String... strings) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(strings[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuilder buffer = new StringBuilder();
                String line = "";

                while ((line = reader.readLine()) != null)
                    buffer.append(line).append("\n");

                w_result = true;

                return buffer.toString();

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null)
                    connection.disconnect();

                try {
                    if (reader != null)
                        reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String wString) {
            super.onPostExecute(wString);

            Pattern pattern = Pattern.compile("temp__value temp__value_with-unit.+span>");
            Matcher matcher = pattern.matcher(wString);

            StringBuilder wString2 = new StringBuilder();
            while (matcher.find()) {
                wString2.append(wString.substring(matcher.start(), matcher.end()));
            }

            Pattern pattern1 = Pattern.compile("[-+]+\\d+\\b");
            Matcher matcher1 = pattern1.matcher(wString2);

            StringBuilder wLink = new StringBuilder();
            if (matcher1.find()) {
                wLink.append(wString2.substring(matcher1.start(), matcher1.end()));
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class GetUrlImgData extends AsyncTask<String,String,String> implements com.example.weatherapp.GetUrlImgData {

        @Override
        protected String doInBackground(String... strings) {
            HttpURLConnection ImgConnection = null;
            BufferedReader readerImg = null;

            try {
                URL imgUrl = new URL(strings[0]);
                ImgConnection = (HttpURLConnection) imgUrl.openConnection();
                ImgConnection.connect();

                InputStream ImgStream = ImgConnection.getInputStream();
                readerImg = new BufferedReader(new InputStreamReader(ImgStream));

                StringBuilder buffer = new StringBuilder();
                String line = "";

                while ((line = readerImg.readLine()) != null)
                    buffer.append(line).append("\n");

                return buffer.toString();

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (ImgConnection != null)
                    ImgConnection.disconnect();

                try {
                    if (readerImg != null)
                        readerImg.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        public void onPostExecute(String imgString){
            super.onPostExecute(imgString);
            Pattern pattern = Pattern.compile("<img.*?n=13");
            Matcher matcher = pattern.matcher(imgString);

            StringBuilder imgString2 = new StringBuilder();
            while (matcher.find()) {
                imgString2.append(imgString.substring(matcher.start(), matcher.end()));
            }

            Pattern pattern1 = Pattern.compile("//.*?n=13");
            Matcher matcher1 = pattern1.matcher(imgString2);

            StringBuilder imgLink = new StringBuilder();
            if (matcher1.find()) {
                imgLink.append(imgString2.substring(matcher1.start(), matcher1.end()));
            }
            String imageUrl = imgLink.toString();
            System.out.println(imageUrl);
            if (imageUrl.length() > 0){

            }
        }
    }
}